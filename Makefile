.PHONY: all bin default dotfiles etc init install

all: bin dotfiles etc init

default: install

dotfiles:
				for file in $(shell find $(CURDIR) -name ".*" -not -name ".gitignore" -not -name ".git" -not -name ".*.swp"); do \
								f=$$(basename $$file); \
								ln -sfn $$file $(HOME)/$$f; \
				done

